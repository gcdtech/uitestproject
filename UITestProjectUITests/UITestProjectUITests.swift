//
//  UITestProjectUITests.swift
//  UITestProjectUITests
//
//  Created by RICHARD MISCHOOK on 01/12/2015.
//  Copyright © 2015 GCD Technologies. All rights reserved.
//

import XCTest

class UITestProjectUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
    
        
        let app = XCUIApplication()
        XCTAssert(app.staticTexts["View 1"].exists, "View 1 label exists on launch")
        
        app.buttons["Move to View 2"].tap()
        XCTAssert(app.staticTexts["View 2"].exists, "View 2 label exists after tap")
        
        
        app.buttons["Back"].tap()
        XCTAssert(app.staticTexts["View 1"].exists, "View 1 label exists after tap")
        
    }
    
}